///<reference path="../main/WatorWorldGridModel.ts"/>

class WorkerClass {
    constructor(context: WorkerGlobalScope) {
        this.m_context = context as DedicatedWorkerGlobalScope;
	    this.m_context.onmessage = (event: MessageEvent) => {
            this.Start();
	    };
    }

    public Start() {
        this.m_context.setTimeout(() => {
        }, 0);
    }

    private m_context: DedicatedWorkerGlobalScope;
}


(( context: WorkerGlobalScope ) => {
	let worker = new WorkerClass(context);
})(self);
