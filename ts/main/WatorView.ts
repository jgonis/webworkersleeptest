///<reference path="UIController.ts"/>
///<reference path="WatorButtonView.ts"/>

class WatorView {
    constructor(context: Window) {
        this.m_context = context;
        this.m_canvas = <HTMLCanvasElement>(this.m_context.document.getElementById("canvas"));
        this.m_numFish = <HTMLInputElement>(this.m_context.document.getElementById("numFish"));
        this.m_fishBreeding = <HTMLInputElement>(this.m_context.document.getElementById("fishBreeding"));
        this.m_numSharks = <HTMLInputElement>(this.m_context.document.getElementById("numSharks"));
        this.m_sharkBreeding = <HTMLInputElement>(this.m_context.document.getElementById("sharkBreeding"));

        this.m_numFishLabel = <HTMLOutputElement>(this.m_context.document.getElementById("numFishLabel"));
        this.m_fishBreedingLabel = <HTMLOutputElement>(this.m_context.document.getElementById("fishBreedingLabel"));
        this.m_numSharksLabel = <HTMLOutputElement>(this.m_context.document.getElementById("numSharksLabel"));
        this.m_sharkBreedingLabel = <HTMLOutputElement>(this.m_context.document.getElementById("sharkBreedingLabel"));

        this.m_worker = new Worker("build/worker.js");
        this.m_controller = new UIController(this, this.m_worker, this.m_context);
        this.m_buttonView = new WatorButtonView(this.m_context, this.m_controller);
        this.SetupEventHandlers();
        this.m_context.setTimeout(()=>{this.HandleCanvasResize();}, 200);
    }

    private SetupEventHandlers() {
        console.log("setting up events");
        this.m_worker.addEventListener("message", (ev: MessageEvent) => {
            this.m_controller.WorkerMessageReceived(ev);
        });

        this.m_sharkBreeding.addEventListener("input", (ev: Event) => {
            this.m_controller.SharkBreedingDragged(ev);
        });

        this.m_numSharks.addEventListener("input", (ev: Event) => {
            this.m_controller.NumSharksDragged(ev);
        });

        this.m_fishBreeding.addEventListener("input", (ev: Event) => {
            this.m_controller.FishBreedingDragged(ev);
        });

        this.m_numFish.addEventListener("input", (ev: Event) => {
            this.m_controller.NumFishDragged(ev);
        });

        this.m_context.window.addEventListener("resize", (event: UIEvent)=>{this.WindowResize(event)});
    }

    private HandleCanvasResize() {
        let dpr = this.m_context.window.devicePixelRatio;
        let cWidth = Math.floor(this.m_canvas.clientWidth * dpr);
        let cHeight = Math.floor(this.m_canvas.clientHeight * dpr);
        if(cWidth !== this.m_canvasWidth || cHeight !== this.m_canvasHeight) {
	        this.m_canvas.width = cWidth;
	        this.m_canvasWidth = cWidth;
	        this.m_canvas.height = cHeight;
	        this.m_canvasHeight = cHeight;
	        this.m_controller.CanvasSizeChanged();
	        this.DoDrawing();
        }
    }

    private WindowResize(event:UIEvent) {
	    this.m_context.requestAnimationFrame(()=>{this.HandleCanvasResize();});
    }

    private DoDrawing() {
        let ctx: CanvasRenderingContext2D | null = this.m_canvas.getContext("2d");
        if (ctx !== null) {
            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.fillRect(0, 0, this.m_canvas.width, this.m_canvas.height);
        }
    }

    public SetSharkBreedingLabel(num: string) {
        this.m_sharkBreedingLabel.value = num;
    }

    public GetSharkBreedingValue(): string {
        return this.m_sharkBreeding.value;
    }

    public SetNumSharksLabel(num: string) {
        this.m_numSharksLabel.value = num;
    }

    public GetNumSharksValue(): string {
        return this.m_numSharks.value;
    }

    public SetNumFishLabel(num: string) {
        this.m_numFishLabel.value = num;
    }

    public GetNumFishValue(): string {
        return this.m_numFish.value;
    }

    public SetFishBreedingLabel(num: string) {
        this.m_fishBreedingLabel.value = num;
    }

    public GetFishBreedingValue(): string {
        return this.m_fishBreeding.value;
    }

	public SetNewSimulationData(idata: ImageData) {
		this.m_imageData = idata;
		this.m_context.requestAnimationFrame(() => {this.DrawNewSimulationData();});
	}

	private DrawNewSimulationData() {
		let ctx: CanvasRenderingContext2D | null = this.m_canvas.getContext("2d");
		if(ctx !== null) {
			ctx.putImageData(this.m_imageData, 0, 0);
		}
	}

	public GetCanvasWidth(): number {
    	    return this.m_canvasWidth;
	}

	public GetCanvasHeight(): number {
    	    return this.m_canvasHeight;
	}

	public DisableControls() {
		this.m_numFish.disabled = true;
		this.m_fishBreeding.disabled = true;
		this.m_numSharks.disabled = true;
		this.m_sharkBreeding.disabled = true;
	}

	public EnableControls() {
		this.m_numFish.disabled = false;
		this.m_fishBreeding.disabled = false;
		this.m_numSharks.disabled = false;
		this.m_sharkBreeding.disabled = false;
	}

    public SetStopEnabled() {
        this.m_buttonView.SetStopEnabled();
    }

    public SetStartDisabled() {
        this.m_buttonView.SetStartDisabled();
    }

    public SetResetDisabled() {
        this.m_buttonView.SetResetDisabled();
    }

    public SetStartEnabled() {
        this.m_buttonView.SetStartEnabled();
    }

    public SetStopDisabled() {
        this.m_buttonView.SetStopDisabled();
    }

    public SetResetEnabled() {
        this.m_buttonView.SetResetEnabled();
    }


    private m_context: Window;
    private m_canvas: HTMLCanvasElement;
    private m_worker: Worker;
    private m_controller: UIController;
    private m_buttonView: WatorButtonView;

    private m_sharkBreeding: HTMLInputElement;
    private m_numSharks: HTMLInputElement;
    private m_fishBreeding: HTMLInputElement;
    private m_numFish: HTMLInputElement;

    private m_sharkBreedingLabel: HTMLOutputElement;
    private m_numSharksLabel: HTMLOutputElement;
    private m_fishBreedingLabel: HTMLOutputElement;
    private m_numFishLabel: HTMLOutputElement;

    private m_imageData: ImageData;

    private m_canvasHeight: number = -1;
    private m_canvasWidth: number = -1;
}