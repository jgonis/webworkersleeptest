///<reference path="WatorView.ts"/>

class AppMain {

	constructor(context: Window) {
        this.m_view = new WatorView(context);
    }

    private m_view: WatorView;
}

((context: Window) => {
    context.onload = () => {
        myInit(context);
    }

    let app: AppMain;

    function myInit(context: Window) {
        app = new AppMain(context);
    }
})(window);