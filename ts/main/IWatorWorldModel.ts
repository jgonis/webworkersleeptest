enum GridOccupant {
    EMPTY = 0,
    FISH,
    SHARK
}

interface IWatorWorldModel {
    IsInitialized(): boolean;
    Step(): void;
    GetWorld(): ImageData;

    InitializeAnimals(numFish: string, numSharks: string): void;
}

function GetRandomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}