///<reference path="WatorView.ts"/>
///<reference path="IWatorWorldModel.ts"/>
///<reference path="WatorWorldMapModel.ts"/>
///<reference path="WatorWorldGridModel.ts"/>

class UIController {
	constructor(view: WatorView, worker: Worker, context: Window) {
		this.m_view = view;
		this.m_worker = worker;
		this.m_context = context;
	}

	public StartClicked(event: MouseEvent) {
		this.m_view.SetStopEnabled();
		this.m_view.SetStartDisabled();
		this.m_view.SetResetDisabled();
		this.m_view.DisableControls();
		this.m_isStopped = false;
		this.m_worker.postMessage("start");
		if(this.m_worldModel.IsInitialized() === false) {
            this.m_worldModel.InitializeAnimals(this.m_view.GetNumFishValue(), this.m_view.GetNumSharksValue());
        }
		this.m_context.setTimeout(() => {
			this.DoSimulationStep();
		}, 0);
	}

	public StopClicked(event: MouseEvent) {
		this.m_view.SetStartEnabled();
		this.m_view.SetStopDisabled();
		this.m_view.SetResetEnabled();
		this.m_isStopped = true;
		this.m_worker.postMessage("stop");
	}

	public ResetClicked(event: MouseEvent) {
		this.m_view.SetResetDisabled();
		this.m_view.SetStopDisabled();
		this.m_view.SetStartEnabled();
		this.m_view.EnableControls();
        this.m_worldModel = new WatorWorldGridModel(this.m_view.GetCanvasWidth(), this.m_view.GetCanvasHeight())
        this.m_view.SetNewSimulationData(this.m_worldModel.GetWorld());
        this.m_worker.postMessage("reset");
	}

	public WorkerMessageReceived(event: MessageEvent) {
		if (this.m_isStopped === false) {
			console.log("received worker message")
			//this.m_view.SetNewSimulationData();
		}
	}

	public SharkBreedingDragged(event: Event) {
		let sharkBreedingInterval: string = this.m_view.GetSharkBreedingValue();
		this.m_view.SetSharkBreedingLabel(sharkBreedingInterval);
	}

	public NumSharksDragged(event: Event) {
		let numSharks: string = this.m_view.GetNumSharksValue();
		this.m_view.SetNumSharksLabel(numSharks);
	}

	public FishBreedingDragged(event: Event) {
		let fishBreedingInterval: string = this.m_view.GetFishBreedingValue();
		this.m_view.SetFishBreedingLabel(fishBreedingInterval);
	}

	public NumFishDragged(event: Event) {
		let numFish: string = this.m_view.GetNumFishValue();
		this.m_view.SetNumFishLabel(numFish);
	}

	public CanvasSizeChanged() {
		this.m_isStopped = true;
		this.m_worker.postMessage("resize");
		this.m_worldModel = new WatorWorldGridModel(this.m_view.GetCanvasWidth(), this.m_view.GetCanvasHeight())
		this.m_view.SetNewSimulationData(this.m_worldModel.GetWorld());
		this.ResetClicked(new MouseEvent("nullEvent"));
	}

	private DoSimulationStep() {
		if (this.m_isStopped === false) {
			this.m_worldModel.Step();
			this.m_view.SetNewSimulationData(this.m_worldModel.GetWorld());
			this.m_context.setTimeout(() => {
                this.DoSimulationStep();
            }, 0);
		}
	}

	private m_view: WatorView;
	private m_worker: Worker;
	private m_context: Window;
	private m_worldModel: IWatorWorldModel;
	private m_isStopped: boolean = true;
}