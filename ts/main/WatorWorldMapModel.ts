///<reference path="IWatorWorldModel.ts"/>

class WatorWorldMapModel implements IWatorWorldModel {

    static readonly SHARK_COLOR: Array<number> = [255, 0, 0];
    static readonly FISH_COLOR: Array<number> = [0, 255, 0];
    static readonly OCEAN_COLOR: Array<number> = [255, 255, 255];
    static readonly CELL_SIZE: number = 4;

    constructor(width: number, height: number) {
        this.fish = new Map<string, number>();
        this.sharks = new Map<string, Array<number>>();
        this.width = width;
        this.height = height;
        this.initialized = false;
        this.fishBreedAge = 5;
        this.sharkBreedAge = 7;
        this.sharkStarveAge = 4;
    }

    public IsInitialized(): boolean {
        return this.initialized;
    }

    public Step(): void {
        this.FishSwimAndBreed();
        this.SharksSwimAndEat();
    }

    public InitializeAnimals(numFish: string, numSharks: string): void {
        this.InitializeFish(Number.parseInt(numFish));
        this.InitializeSharks(Number.parseInt(numSharks));
        this.initialized = true;
    }

    public GetWorld(): ImageData {
        let id = new ImageData(this.width, this.height);
        id.data.fill(255);
        let vals: Array<string>;
        for (let entry of this.fish) {
            vals = entry[0].split(',');
            this.SetColor(WatorWorldGridModel.FISH_COLOR[0], WatorWorldGridModel.FISH_COLOR[1],
                WatorWorldGridModel.FISH_COLOR[2], Number.parseInt(vals[0]), Number.parseInt(vals[1]), id.data);
        }
        for (let entry of this.sharks) {
            vals = entry[0].split(',');
            this.SetColor(WatorWorldGridModel.SHARK_COLOR[0], WatorWorldGridModel.SHARK_COLOR[1],
                WatorWorldGridModel.SHARK_COLOR[2], Number.parseInt(vals[0]), Number.parseInt(vals[1]), id.data);
        }
        return id;
    }

    public AtCellCoords(coordString: string): GridOccupant {
        if(this.fish.has(coordString) === true) {
            return GridOccupant.FISH
        } else if(this.sharks.has(coordString) === true) {
            return GridOccupant.SHARK;
        } else {
            return GridOccupant.EMPTY;
        }
    }

    private FishSwimAndBreed() {
        const validNextCoords: Array<string> = ['', '', '', '', '', '', '', ''];
        let tempCoord: string = '';
        const currentCoords: Array<number> = [0, 0];

        let keys: Array<string> = Array.from(this.fish.keys());
        for(const key of keys) {
            let numberOfValidCoords = 0;
            const coords = key.split(',');
            currentCoords[0] = Number.parseInt(coords[0]);
            currentCoords[1] = Number.parseInt(coords[1]);
            for(let i = 0; i < this.coordTransforms.length; i++) {
                tempCoord = this.NormalizeCoords((currentCoords[0] + this.coordTransforms[i][0]), (currentCoords[1] + this.coordTransforms[i][1]));
                if(this.AtCellCoords(tempCoord) === GridOccupant.EMPTY) {
                    validNextCoords[numberOfValidCoords] = tempCoord;
                    numberOfValidCoords++;
                }
            }

            let age = this.fish.get(key);
            if(age !== undefined) {
                age = age + 1
            } else {
                age = -1;
                throw new Error("bad index!");
            }

            if(numberOfValidCoords !== 0) {
                const newCoordIndex = GetRandomInt(0, numberOfValidCoords);
                if(age >= this.fishBreedAge) {
                    this.SetFish(validNextCoords[newCoordIndex], 0);
                    this.SetFish(key, 0);
                } else {
                    this.SetFish(validNextCoords[newCoordIndex], age);
                    this.fish.delete(key);
                }
            } else {
                this.SetFish(key, age);
            }
        }
    }

    private SharksSwimAndEat() {
        const validMoveCoords: Array<string> = ['', '', '', '', '', '', '', ''];
        const validEatCoords: Array<string> = ['', '', '', '', '', '', '', ''];

        let keys: Array<string> = Array.from(this.sharks.keys());
        for(const key of keys) {
            let shark = this.sharks.get(key);
            if (shark !== undefined && shark[1] >= this.sharkStarveAge) {
                this.sharks.delete(key);
            } else {
                this.SharksEatSwimAndBreed(key, validMoveCoords, validEatCoords);
            }
        }
    }

    private SharksEatSwimAndBreed(key: string, validMoveCoords: Array<string>, validEatCoords: Array<string>) {
        let tempCoord: string = '';
        const currentCoords: Array<number> = [0, 0];
        let numberOfValidMoveCoords = 0;
        let numberOfValidEatCoords = 0;
        const coords = key.split(',');
        currentCoords[0] = Number.parseInt(coords[0]);
        currentCoords[1] = Number.parseInt(coords[1]);
        for (let i = 0; i < this.coordTransforms.length; i++) {
            tempCoord = this.NormalizeCoords((currentCoords[0] + this.coordTransforms[i][0]), (currentCoords[1] + this.coordTransforms[i][1]));
            if (this.AtCellCoords(tempCoord) === GridOccupant.EMPTY) {
                validMoveCoords[numberOfValidMoveCoords] = tempCoord;
                numberOfValidMoveCoords++;
            } else if (this.AtCellCoords(tempCoord) === GridOccupant.FISH) {
                validEatCoords[numberOfValidEatCoords] = tempCoord;
                numberOfValidEatCoords++;
            }
        }
        const val = this.sharks.get(key);
        let age: number = -1;
        let timeSinceAte: number = -1;
        if (val === undefined) {
            throw new Error("bad index!");
        } else {
            age = val[0];
            timeSinceAte = val[1];
        }

        if (numberOfValidEatCoords !== 0) {
            const newCoordIndex = GetRandomInt(0, numberOfValidEatCoords);
            if( age >= this.sharkBreedAge) {
              this.SetShark(validEatCoords[newCoordIndex], [0, 0]);
              this.SetShark(key, [0, 0]);
              this.fish.delete(validEatCoords[newCoordIndex]);
            } else {
                this.SetShark(validEatCoords[newCoordIndex], [age + 1, 0]);
                this.sharks.delete(key);
                this.fish.delete(validEatCoords[newCoordIndex]);
            }
        } else if (numberOfValidMoveCoords !== 0) {
            const newCoordIndex = GetRandomInt(0, numberOfValidEatCoords);
            if(age >= this.sharkBreedAge) {
                this.SetShark(validMoveCoords[newCoordIndex], [0, timeSinceAte + 1]);
                this.SetShark(key, [0, 0]);
            } else {
                this.SetShark(validMoveCoords[newCoordIndex], [age + 1, timeSinceAte + 1]);
                this.sharks.delete(key);
            }
        } else {
            this.SetShark(key, [age + 1, timeSinceAte + 1]);
        }
    }

    private SetShark(coordString: string, ageAndEatTime: Array<number>): void {
        this.sharks.set(coordString, ageAndEatTime);
    }

    private SetFish(coordString: string, age: number): void {
        this.fish.set(coordString, age);
    }

    private InitializeFish(numFish: number) {
        this.numberOfFish = numFish;
        while (numFish !== 0) {
            const coord = GetRandomInt(0, this.width).toString() + ',' + GetRandomInt(0, this.height).toString();
            if(this.fish.has(coord) === false
                && this.sharks.has(coord) === false) {
                this.SetFish(coord, 0);
                numFish = numFish - 1;
            }
        }
    }

    private InitializeSharks(numSharks: number) {
    	while(numSharks !== 0) {
            const coord = GetRandomInt(0, this.width).toString() + ',' + GetRandomInt(0, this.height).toString();
            if(this.fish.has(coord) === false
                && this.sharks.has(coord) === false) {
                this.SetShark(coord, [0, 0]);
                numSharks = numSharks - 1;
            }
    	}
    }

    private NormalizeY(y: number): number {
        if(y < 0)
            return (this.height) - (y % this.height);
        else if(y >= this.height)
            return y - this.height;
        else return y;
    }

    private NormalizeX(x: number): number {
        if(x < 0)
            return this.width - x;
        else if(x >= this.width)
            return x - this.width;
        else return x;
    }

    private NormalizeCoords(x: number, y: number): string {
        return this.NormalizeX(x) + ',' + this.NormalizeY(y);
    }

    private CoordsToIndex(x: number, y: number): number {
        return (x * WatorWorldGridModel.CELL_SIZE) + (y * (this.width * WatorWorldGridModel.CELL_SIZE));
    }

    private SetColor(r: number, g: number, b: number, x: number, y: number, world: Uint8ClampedArray) {
        const startIndex = this.CoordsToIndex(x, y);
        world[startIndex] = r;
        world[startIndex + 1] = g;
        world[startIndex + 2] = b;
        world[startIndex + 3] = 255; // full alpha for full opacity;
    }

    private coordTransforms: Array<Array<number>> = [[0,-1], [1,-1], [1,0], [1,1], [0,1],[-1,1],[-1,0],[-1,-1]];
    private fish: Map<string, number>;
    private sharks: Map<string, Array<number>>;
    private width: number;
    private height: number;
    private initialized: boolean;
    private numberOfFish: number;
    private fishBreedAge: number;
    private sharkBreedAge: number;
    private sharkStarveAge: number;
}