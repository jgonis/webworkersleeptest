///<reference path="IWatorWorldModel.ts"/>

class WatorWorldGridModel implements IWatorWorldModel {

    static readonly SHARK_COLOR: Array<number> = [255, 0, 0];
    static readonly FISH_COLOR: Array<number> = [0, 255, 0];
    static readonly OCEAN_COLOR: Array<number> = [255, 255, 255];
    static readonly CELL_SIZE: number = 4;

    constructor(width: number, height: number) {
        this.world1 = new Uint8ClampedArray(width * height * WatorWorldGridModel.CELL_SIZE);
        this.world2 = new Uint8ClampedArray(width * height * WatorWorldGridModel.CELL_SIZE);
        this.currentWorld = this.world1;
        this.width = width;
        this.height = height;
        this.interval = this.width * WatorWorldGridModel.CELL_SIZE;
        this.initialized = false;
        this.fishBreedAge = 5;
        this.sharkBreedAge = 10;
        this.sharkStarveAge = 3;
        this.InitOcean();
    }

    private InitOcean() {
        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                this.SetEmpty(x, y, this.world1);
                this.SetEmpty(x, y, this.world2);
            }
        }
    }

    public IsInitialized(): boolean {
        return this.initialized;
    }

    public Step(): void {
        let sharkCount = 0;
        let nextWorld: Uint8ClampedArray;
        if (this.currentWorld === this.world1)
            nextWorld = this.world2;
        else
            nextWorld = this.world1;
        const coords: Array<number> = [0, 0];
        let occupant: GridOccupant;
        for (let i = 0; i < this.currentWorld.length; i = i + WatorWorldGridModel.CELL_SIZE) {
            //Fish swim and Breed
            occupant = this.AtCellIndex(i, this.currentWorld);
            if (occupant === GridOccupant.FISH) {
                this.IndexToCoords(i, coords);
                this.FishSwimAndBreed(coords, nextWorld);
            } else if (occupant === GridOccupant.SHARK) {
                this.IndexToCoords(i, coords);
                this.SharksSwimAndEat(coords, nextWorld);
                sharkCount++;
            }
        }
        this.currentWorld = nextWorld;
        //console.log("sharkCount:" + sharkCount);
    }

    public InitializeAnimals(numFish: string, numSharks: string): void {
        this.InitializeFish(Number.parseInt(numFish));
        this.InitializeSharks(Number.parseInt(numSharks));
        this.initialized = true;
    }

    public GetWorld(): ImageData {
        return new ImageData(this.currentWorld, this.width, this.height);
    }

    public AtCellCoords(x: number, y: number): GridOccupant {
        return this.AtCellCoordsInWorld(x, y, this.currentWorld);
    }

    private FishSwimAndBreed(coords: Array<number>, nextWorld: Uint8ClampedArray) {
        const validNextCoords: Array<Array<number>> = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]];
        let tempCoord: Array<number> = [0, 0];
        let numberOfValidCoords = 0;

        for (let i = 0; i < this.coordTransforms.length; i++) {
            this.NormalizeCoords((coords[0] + this.coordTransforms[i][0]), (coords[1] + this.coordTransforms[i][1]), tempCoord);
            if (this.AtCellCoordsInWorld(tempCoord[0], tempCoord[1], nextWorld) === GridOccupant.EMPTY
                && this.AtCellCoordsInWorld(tempCoord[0], tempCoord[1], this.currentWorld) === GridOccupant.EMPTY) {
                validNextCoords[numberOfValidCoords][0] = tempCoord[0];
                validNextCoords[numberOfValidCoords][1] = tempCoord[1];
                numberOfValidCoords++;
            }
        }

        let age = this.GetAge(coords[0], coords[1], this.currentWorld);
        if (numberOfValidCoords !== 0) {
            const newCoordIndex = GetRandomInt(0, numberOfValidCoords);
            if (age >= this.fishBreedAge) {
                this.SetFish(validNextCoords[newCoordIndex][0], validNextCoords[newCoordIndex][1], 0, nextWorld);
                this.SetFish(coords[0], coords[1], 0, nextWorld);
            } else {
                if (age < 255)
                    age++;
                this.SetFish(validNextCoords[newCoordIndex][0], validNextCoords[newCoordIndex][1], age, nextWorld);
            }
        } else {
            if (age < 255)
                age++;
            this.SetFish(coords[0], coords[1], age, nextWorld);
        }
        this.SetEmpty(coords[0], coords[1], this.currentWorld);
    }

    private SharksSwimAndEat(coords: Array<number>, nextWorld: Uint8ClampedArray) {
        const validMoveCoords: Array<Array<number>> = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]];
        const validEatCoords: Array<Array<number>> = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]];

        const timeSinceLastAte = this.GetTimeSinceAte(coords[0], coords[1], this.currentWorld);
        //console.log(timeSinceLastAte);
        if (timeSinceLastAte < this.sharkStarveAge) {
            this.SharksEatSwimAndBreed(coords, validMoveCoords, validEatCoords, nextWorld);
        } else {
            //console.log(timeSinceLastAte);
            this.SetEmpty(coords[0], coords[1], this.currentWorld);
            this.SetEmpty(coords[0], coords[1], nextWorld);
        }
    }

    private SharksEatSwimAndBreed(coords: Array<number>, validMoveCoords: Array<Array<number>>, validEatCoords: Array<Array<number>>, nextWorld: Uint8ClampedArray) {
        let tempCoord: Array<number> = [0,0];
        let numberOfValidMoveCoords = 0;
        let numberOfValidEatCoords = 0;
        let nextOccupant: GridOccupant;
        let currentOccupant: GridOccupant;

        for (let i = 0; i < this.coordTransforms.length; i++) {
            this.NormalizeCoords((coords[0] + this.coordTransforms[i][0]), (coords[1] + this.coordTransforms[i][1]), tempCoord);
            nextOccupant = this.AtCellCoordsInWorld(tempCoord[0], tempCoord[1], nextWorld);
            currentOccupant = this.AtCellCoordsInWorld(tempCoord[0], tempCoord[1], this.currentWorld);
            if ( nextOccupant === GridOccupant.EMPTY && currentOccupant === GridOccupant.EMPTY) {
                validMoveCoords[numberOfValidMoveCoords][0] = tempCoord[0];
                validMoveCoords[numberOfValidMoveCoords][1] = tempCoord[1];
                numberOfValidMoveCoords++;
            } else if (nextOccupant === GridOccupant.FISH
                || (currentOccupant === GridOccupant.FISH) ) {
                validEatCoords[numberOfValidEatCoords][0] = tempCoord[0];
                validEatCoords[numberOfValidEatCoords][1] = tempCoord[1];
                numberOfValidEatCoords++;
            }
        }

        let age: number = this.GetAge(coords[0], coords[1], this.currentWorld);
        let timeSinceAte: number = this.GetTimeSinceAte(coords[0], coords[1], this.currentWorld);

        if (numberOfValidEatCoords !== 0) {
            const newCoordIndex = GetRandomInt(0, numberOfValidEatCoords);
            if( age >= this.sharkBreedAge) {
                this.SetShark(validEatCoords[newCoordIndex][0], validEatCoords[newCoordIndex][1], 0, 0, nextWorld);
                this.SetShark(coords[0], coords[1], 0, 0, nextWorld);
            } else {
                this.SetShark(validEatCoords[newCoordIndex][0], validEatCoords[newCoordIndex][1], age + 1, 0, nextWorld);
            }
            this.SetEmpty(validEatCoords[newCoordIndex][0], validEatCoords[newCoordIndex][1], this.currentWorld);
        } else if (numberOfValidMoveCoords !== 0) {
            const newCoordIndex = GetRandomInt(0, numberOfValidEatCoords);
            if(age >= this.sharkBreedAge) {
                this.SetShark(validMoveCoords[newCoordIndex][0], validMoveCoords[newCoordIndex][1], 0, timeSinceAte + 1, nextWorld);
                this.SetShark(coords[0], coords[1],0,0, nextWorld);
            } else {
                this.SetShark(validMoveCoords[newCoordIndex][0], validMoveCoords[newCoordIndex][1],age + 1, timeSinceAte + 1, nextWorld);
            }
        } else {
            this.SetShark(coords[0], coords[1], age + 1, timeSinceAte + 1, nextWorld);
        }
        this.SetEmpty(coords[0], coords[1], this.currentWorld);
    }

    private SetShark(x: number, y: number, age: number, timeSinceAte: number, world: Uint8ClampedArray): void {
        this.SetColor(WatorWorldGridModel.SHARK_COLOR[0], WatorWorldGridModel.SHARK_COLOR[1],
            age, 255 - timeSinceAte, x, y, world);
    }

    private SetFish(x: number, y: number, age: number, world: Uint8ClampedArray): void {
        this.SetColor(WatorWorldGridModel.FISH_COLOR[0], WatorWorldGridModel.FISH_COLOR[1],
            age, 255, x, y, world);
    }

    private SetEmpty(x: number, y: number, world: Uint8ClampedArray): void {
        this.SetColor(WatorWorldGridModel.OCEAN_COLOR[0], WatorWorldGridModel.OCEAN_COLOR[1],
            WatorWorldGridModel.OCEAN_COLOR[2], 255, x, y, world);
    }

    private AtCellCoordsInWorld(x: number, y: number, world: Uint8ClampedArray): GridOccupant {
        const startIndex = this.CoordsToIndex(x, y);
        if (world[startIndex] === WatorWorldGridModel.OCEAN_COLOR[0]
            && world[startIndex + 1] === WatorWorldGridModel.OCEAN_COLOR[1]
            && world[startIndex + 2] === WatorWorldGridModel.OCEAN_COLOR[2]) {
            return GridOccupant.EMPTY;
        }
        else if (world[startIndex] === WatorWorldGridModel.FISH_COLOR[0]
            && world[startIndex + 1] === WatorWorldGridModel.FISH_COLOR[1]
            && world[startIndex + 2] === WatorWorldGridModel.FISH_COLOR[2]) {
            return GridOccupant.FISH;
        }
        else
            return GridOccupant.SHARK;
    }

    private GetAge(x: number, y: number, world: Uint8ClampedArray): number {
        const startIndex = this.CoordsToIndex(x, y);
        return world[startIndex + 2];
    }

    private GetTimeSinceAte(x: number, y: number, world: Uint8ClampedArray): number {
        const startIndex = this.CoordsToIndex(x, y);
        return 255 - world[startIndex + 3];
    }

    private AtCellIndex(startIndex: number, world: Uint8ClampedArray): GridOccupant {
        if (world[startIndex] === WatorWorldGridModel.OCEAN_COLOR[0]
            && world[startIndex + 1] === WatorWorldGridModel.OCEAN_COLOR[1]
            && world[startIndex + 2] === WatorWorldGridModel.OCEAN_COLOR[2]) {
            return GridOccupant.EMPTY;
        }
        else if (world[startIndex] === WatorWorldGridModel.FISH_COLOR[0]
            && world[startIndex + 1] === WatorWorldGridModel.FISH_COLOR[1]) {
            return GridOccupant.FISH;
        }
        else
            return GridOccupant.SHARK;
    }

    private InitializeFish(numFish: number) {
        let numberOfFishToGo = numFish;
        while (numberOfFishToGo !== 0) {
            const x: number = GetRandomInt(0, this.width);
            const y: number = GetRandomInt(0, this.height);
            if (this.AtCellCoordsInWorld(x, y, this.currentWorld) === GridOccupant.EMPTY) {
                this.SetFish(x, y, GetRandomInt(0, this.fishBreedAge), this.currentWorld);
                numberOfFishToGo = numberOfFishToGo - 1;
            }
        }
    }

    private InitializeSharks(numSharks: number) {
        let numberOfSharksToGo = numSharks;
        while (numberOfSharksToGo !== 0) {
            let x: number = GetRandomInt(0, this.width);
            let y: number = GetRandomInt(0, this.height);
            if (this.AtCellCoordsInWorld(x, y, this.currentWorld) === GridOccupant.EMPTY) {
                this.SetShark(x, y, GetRandomInt(0, this.sharkBreedAge), 0, this.currentWorld);
                numberOfSharksToGo = numberOfSharksToGo - 1;
            }
        }
    }

    private NormalizeY(y: number): number {
        if (y < 0)
            return this.height + y ;
        else if (y >= this.height)
            return y - this.height;
        else return y;
    }

    private NormalizeX(x: number): number {
        if (x < 0)
            return ( this.width ) + x;
        else if (x >= this.width)
            return x - this.width;
        else return x;
    }

    private NormalizeCoords(x: number, y: number, normalizedCoords: Array<number>) {
        normalizedCoords[0] = this.NormalizeX(x);
        normalizedCoords[1] = this.NormalizeY(y);
    }

    private IndexToCoords(i: number, coords: Array<number>) {
        coords[0] = (i % (this.interval)) / WatorWorldGridModel.CELL_SIZE;
        coords[1] = Math.trunc(i / this.interval);
    }

    private CoordsToIndex(x: number, y: number): number {
        return (x * WatorWorldGridModel.CELL_SIZE) + (y * (this.width * WatorWorldGridModel.CELL_SIZE));
    }

    private SetColor(r: number, g: number, b: number, a: number, x: number, y: number, world: Uint8ClampedArray) {
        const startIndex = this.CoordsToIndex(x, y);
        world[startIndex] = r;
        world[startIndex + 1] = g;
        world[startIndex + 2] = b;
        world[startIndex + 3] = a; // full alpha for full opacity;
    }

    private coordTransforms: Array<Array<number>> = [[0, -1], [1, -1], [1, 0], [1, 1], [0, 1], [-1, 1], [-1, 0], [-1, -1]];
    private world1: Uint8ClampedArray;
    private world2: Uint8ClampedArray;
    private currentWorld: Uint8ClampedArray;
    private width: number;
    private height: number;
    private interval: number;
    private initialized: boolean;
    private fishBreedAge: number;
    private sharkBreedAge: number;
    private sharkStarveAge: number;
}