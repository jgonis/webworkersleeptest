///<reference path="UIController.ts"/>

class WatorButtonView {
    constructor(context: Window, controller: UIController) {
        this.m_context = context;
        this.m_startButton = <HTMLButtonElement>(this.m_context.document.getElementById("startButton"));
        this.m_stopButton = <HTMLButtonElement>(this.m_context.document.getElementById("stopButton"));
        this.m_resetButton = <HTMLButtonElement>(this.m_context.document.getElementById("resetButton"));
        this.m_controller = controller;
        this.m_stopButton.disabled = true;
        this.m_resetButton.disabled = true;
        this.SetupEventHandlers();
    }

    private SetupEventHandlers() {
        console.log("setting up events");
        this.m_startButton.addEventListener("click", (event: MouseEvent) => {
            this.m_controller.StartClicked(event);
        });
        this.m_stopButton.addEventListener("click", (event: MouseEvent) => {
            this.m_controller.StopClicked(event);
        });
        this.m_resetButton.addEventListener("click", (event: MouseEvent) => {
            this.m_controller.ResetClicked(event);
        });
    }

    public SetStopEnabled() {
        this.m_stopButton.disabled = false;
    }

    public SetStartDisabled() {
        this.m_startButton.disabled = true;
    }

    public SetResetDisabled() {
        this.m_resetButton.disabled = true;
    }

    public SetStartEnabled() {
        this.m_startButton.disabled = false;
    }

    public SetStopDisabled() {
        this.m_stopButton.disabled = true;
    }

    public SetResetEnabled() {
        this.m_resetButton.disabled = false;
    }

    private m_context: Window;
    private m_controller: UIController;
    private m_startButton: HTMLButtonElement;
    private m_stopButton: HTMLButtonElement;
    private m_resetButton: HTMLButtonElement;
}